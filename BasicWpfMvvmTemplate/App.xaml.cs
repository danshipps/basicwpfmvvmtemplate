﻿using System;
using System.Windows;

namespace BasicWpfMvvmTemplate
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private void MyStartup(object sender, StartupEventArgs e)
        {
            var arguments = Environment.GetCommandLineArgs();
            var startupString = string.Empty;

            if (arguments.GetLength(0) > 1)
            {
                //You can pre-process the command line string here...
            }
            Model.RunApp(startupString);
        }
    }
}
