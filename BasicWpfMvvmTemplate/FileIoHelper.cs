﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using Ookii.Dialogs.Wpf;

namespace BasicWpfMvvmTemplate
{
    internal static class FileIoHelper
    {
        internal static string SelectExistingFileGetPath(string defaultExt = null, string filter = null, string startingDir = null)
        {
            var dialog = new Microsoft.Win32.OpenFileDialog { DefaultExt = defaultExt, Filter = filter, InitialDirectory = startingDir };
            var result = dialog.ShowDialog();
            return result == true ? dialog.FileName : string.Empty;
        }

        internal static string SelectNewFileGetPath(string defaultExt = null, string filter = null, string startingDir = null)
        {
            var dialog = new Microsoft.Win32.SaveFileDialog { DefaultExt = defaultExt, Filter = filter, InitialDirectory = startingDir };
            var result = dialog.ShowDialog();
            return result == true ? dialog.FileName : string.Empty;
        }

        internal static string SelectFolderGetPath(string startingDir = null, string description = "", bool useDescriptionAsTitle = true)
        {
            var dialog = new VistaFolderBrowserDialog
            {
                SelectedPath = startingDir,
                Description = description,
                UseDescriptionForTitle = useDescriptionAsTitle
            };
            var result = dialog.ShowDialog();
            return result == true ? dialog.SelectedPath : string.Empty;
        }

        //internal static void OpenMicrosoftExcel(string file)
        //{
        //    try
        //    {
        //        var startInfo = new ProcessStartInfo { FileName = "EXCEL.EXE", Arguments = file };
        //        Process.Start(startInfo);
        //    }
        //    catch (InvalidOperationException invalidOperationException)
        //    {
        //        //TODO: Catch and handle this
        //    }
        //    catch (ArgumentNullException argumentNullException)
        //    {
        //        //TODO: Catch and handle this
        //    }
        //    catch (FileNotFoundException fileNotFoundException)
        //    {
        //        //TODO: Catch and handle this
        //    }
        //    catch (Win32Exception win32Exception)
        //    {
        //        //TODO: Catch and handle this
        //    }
        //    catch (Exception e)
        //    {
        //        //TODO: Catch and handle this
        //    }
        //}
    }
}
