﻿using System.Windows;

namespace BasicWpfMvvmTemplate
{
    internal static class Model
    {
        internal static ViewModel ViewModel { get; private set; }
        internal static View View { get; private set; }

        private static string _csvFilePath;
        internal static string CsvFilePath { get { return _csvFilePath; } set { _csvFilePath = value; ViewModel.CsvFilePath = _csvFilePath; } }

        static Model()
        {
            ViewModel = new ViewModel();
            View = new View {DataContext = ViewModel};
        }

        internal static void RunApp(string startupString = null)
        {
            if (!string.IsNullOrEmpty(startupString))
            {
                //You can do something with the command line argument string here
            }

            BindAndShowWindow();
        }

        private static void BindAndShowWindow(Window w = null)
        {
            if (w == null) w = View;

            w.Show();
            w.Focus();
        }
    }
}
