# README #

This is a basic C# WPF MVVM Project Template for Visual Studio 2013. It provides a good starting point with command templates and basic file IO functions. There are also some simple data binding examples.

This project uses the Extended WPF Toolkit package and the Ookii.Dialogs package.

If you have any suggestions I would love to hear them! :)

### What is this repository for? ###

* Basic C# WPF MVVM Template
* Version 1.0

### How do I get set up? ###

To “install” the template copy the zip file to this directory: %USERPROFILE%\Documents\Visual Studio 2013\Templates\ProjectTemplates

There is no need to upzip the file.

### Who do I talk to? ###

* Repo owner