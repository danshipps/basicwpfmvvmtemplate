﻿namespace BasicWpfMvvmTemplate
{
    internal static class Constants
    {
        internal const string SomeAwesomeConstant = @"TheNameOfAThingMaybe?";

        internal const string CsvFileDefaultExt = @".csv";
        internal const string CsvFileFilter = @"CSV Files (*.csv)|*.csv";
    }
}
