﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Input;
using BasicWpfMvvmTemplate.Annotations;

namespace BasicWpfMvvmTemplate
{
    internal class ViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        //Public Command Properties
        //Menu Only Commands
        public ICommand ExitCommand { get { return _exitCommand ?? (_exitCommand = new DelegateCommand(Execute_ExitCommand, CanExecute_ExitCommand)); } }
        //Other Commands
        public ICommand CsvSetPathCommand { get { return _csvSetPathCommand ?? (_csvSetPathCommand = new DelegateCommand(Execute_SetCsvFilePathCommand, CanExecute_SetCsvFilePathCommand)); } }

        //Private Command Backing Fields
        //Menu Only Commands
        private ICommand _exitCommand;
        //Other Commands
        private ICommand _csvSetPathCommand;

        public BindingList<string> ListOfStringBindingList { get { return _listOfStringBindingList; } set { _listOfStringBindingList = value; OnPropertyChanged(); } }
        public string StringBinding { get { return _stringBinding; } set { _stringBinding = value; OnPropertyChanged(); } }
        public string CsvFilePath { get { return _csvFilePath; } set { _csvFilePath = value; OnPropertyChanged(); } }

        private BindingList<string> _listOfStringBindingList;
        private string _stringBinding;
        private string _csvFilePath;


        internal ViewModel()
        {
            InitAllFields();
        }

        private void InitAllFields()
        {
            ListOfStringBindingList = new BindingList<string> {"Item 1", "Item 2", "Item 3"};
            StringBinding = "A string example";
        }

        //Exit Command
        internal void Execute_ExitCommand(object parameter)
        {
            //TODO: Fill This Out
            MessageBox.Show("Exit");
        }
        internal bool CanExecute_ExitCommand(object parameter)
        {
            return true;
        }

        //SetLimitFilePathCommand
        internal void Execute_SetCsvFilePathCommand(object parameter)
        {
            var newPath = FileIoHelper.SelectExistingFileGetPath(Constants.CsvFileDefaultExt, Constants.CsvFileFilter);
            if (!string.IsNullOrEmpty(newPath)) Model.CsvFilePath = newPath;
        }
        internal bool CanExecute_SetCsvFilePathCommand(object parameter)
        {
            return true;
        }
    }
}
